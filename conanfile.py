from conan import ConanFile
from conan.errors import ConanInvalidConfiguration
from conan.tools.microsoft import check_min_vs, is_msvc_static_runtime, is_msvc
from conan.tools.files import apply_conandata_patches, export_conandata_patches, get, copy, rm, rmdir, replace_in_file
from conan.tools.build import check_min_cppstd
from conan.tools.scm import Version
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain, cmake_layout
from conan.tools.env import VirtualBuildEnv
import os

required_conan_version = ">=1.53.0"

class LibmariadbConan(ConanFile):
    name = "libmariadb"
    description = "a community developed fork of MySQL server."
    license = "GPL-2.0"
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "https://github.com/MariaDB/server"
    topics = ("mariadb", "mysql", "sql")
    package_type = "library"
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
    }

    def export_sources(self):
        export_conandata_patches(self)

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")
        self.settings.rm_safe("compiler.libcxx")
        self.settings.rm_safe("compiler.cppstd")

    def layout(self):
        cmake_layout(self, src_folder="src")

    def requirements(self):
         self.requires("zlib/[>=1.2.11 <2]")

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["WITHOUT_SERVER"] = False
        tc.variables["DISABLE_SHARED"] = not self.options.shared
        tc.variables["WITH_EMBEDDED_SERVER"] = True
        tc.variables["WITH_UNITTESTS"] = False
        tc.variables["WITH_UNIT_TESTS"] = False
        tc.variables["WITH_SSL"] = False
        tc.variables["WITU_CURL"] = False
        tc.variables["WITU_EXTERNAL_ZLIB"] = True
        tc.generate()
        deps = CMakeDeps(self)
        deps.generate()

    def build(self):
        apply_conandata_patches(self)
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        copy(self, pattern="LICENSE", dst=os.path.join(self.package_folder, "licenses"), src=self.source_folder)
        cmake = CMake(self)
        cmake.install()

        # self.copy("*.h", dst="include", src="mariadb/libmariadb/include/")
        # self.copy("*.h", dst="include", src="libmariadb/include/")
        # self.copy("libmariadbclient.lib", dst="lib", keep_path=False)
        # self.copy("libmariadbclient.dylib", dst="lib", keep_path=False)
        # self.copy("*.a", dst="lib", src="libmariadb/libmariadb", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["mariadbclient"]
        if self.settings.os in ["Linux", "FreeBSD"]:
            self.cpp_info.system_libs.append("pthread")
        if self.settings.os == "Linux":
            self.cpp_info.system_libs.append("dl")
